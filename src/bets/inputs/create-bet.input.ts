/* eslint-disable @typescript-eslint/no-unused-vars */
import { Field, Float, InputType, Int } from '@nestjs/graphql';

@InputType()
export class CreateBetInput {
  @Field((type) => Int)
  userId: number;

  @Field((type) => Float)
  betAmount: number;

  @Field((type) => Float)
  chance: number;
}
