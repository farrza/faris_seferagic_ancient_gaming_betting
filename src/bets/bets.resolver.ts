import { NotFoundException } from '@nestjs/common';
import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';
import { Bet } from './models/bet.model';
import { BetsService } from './bets.service';
import { CreateBetInput } from './inputs/create-bet.input';

@Resolver()
export class BetsResolver {
  constructor(private readonly betsService: BetsService) {}

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  @Query((returns) => Bet)
  async getBet(@Args('id') id: number): Promise<Bet> {
    try {
      const bet = await this.betsService.getBet(id);
      if (!bet) {
        throw new NotFoundException(id);
      }
      return bet;
    } catch (error) {
      console.log(`[getBet Resolver Error]: `, error);
      throw error;
    }
  }
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  @Query((returns) => [Bet])
  async getBetList(): Promise<Bet[]> {
    try {
      const bets = await this.betsService.getBetList();
      if (!bets) {
        throw new NotFoundException('No Bets found!');
      }
      return bets;
    } catch (error) {
      console.log(`[getBetList Resolver Error]: `, error);
      throw error;
    }
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  @Query((returns) => [Bet])
  async getBestBetPerUser(@Args('limit') limit: number): Promise<Bet[]> {
    try {
      const bets = await this.betsService.getBestBetPerUser(limit);
      if (!bets) {
        throw new NotFoundException('No bets found!');
      }
      return bets;
    } catch (error) {
      console.log(`[getBestBetPerUser Resolver Error]: `, error);
      throw error;
    }
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  @Mutation((returns) => Bet)
  async createBet(
    @Args('createBetInput') createBetInput: CreateBetInput,
  ): Promise<Bet> {
    try {
      return await this.betsService.createBet(createBetInput);
    } catch (error) {
      console.log(`[createBet Resolver Error]: `, error);
      throw error;
    }
  }
}
