/* eslint-disable @typescript-eslint/no-unused-vars */
import { Field, Float, ID, ObjectType } from '@nestjs/graphql';

@ObjectType({ description: 'bet' })
export class Bet {
  @Field((type) => ID)
  id: number;

  @Field()
  userId: number;

  @Field((type) => Float)
  betAmount: number;

  @Field((type) => Float)
  chance: number;

  @Field((type) => Float)
  payout: number;

  @Field()
  win: boolean;
}
