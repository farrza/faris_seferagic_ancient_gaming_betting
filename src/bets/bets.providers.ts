import { BETS_REPOSITORY } from 'src/constants';
import { Bet } from './bet.entity';

export const betsProviders = [
  {
    provide: BETS_REPOSITORY,
    useValue: Bet,
  },
];
