import { Module } from '@nestjs/common';
import { DatabaseModule } from 'src/database/database.module';
import { usersProviders } from 'src/users/users.providers';
import { betsProviders } from './bets.providers';
import { BetsResolver } from './bets.resolver';
import { BetsService } from './bets.service';

@Module({
  imports: [DatabaseModule],
  providers: [BetsResolver, BetsService, ...betsProviders, ...usersProviders],
})
export class BetsModule {}
