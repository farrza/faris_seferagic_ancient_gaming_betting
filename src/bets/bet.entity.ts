import {
  Table,
  Column,
  Model,
  DataType,
  ForeignKey,
  BelongsTo,
} from 'sequelize-typescript';
import { User } from 'src/users/user.entity';

@Table
export class Bet extends Model<Bet> {
  @Column({
    primaryKey: true,
    autoIncrement: true,
  })
  id: number;

  @ForeignKey(() => User)
  @Column
  userId: number;

  @BelongsTo(() => User)
  user: User;

  @Column(DataType.DECIMAL(10, 2))
  betAmount: number;

  @Column(DataType.DECIMAL(10, 2))
  chance: number;

  @Column(DataType.DECIMAL(10, 2))
  payout: number;

  @Column
  win: boolean;
}
