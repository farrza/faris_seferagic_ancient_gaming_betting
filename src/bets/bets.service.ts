import { Inject, Injectable } from '@nestjs/common';
import { Bet } from './bet.entity';
import { CreateBetInput } from './inputs/create-bet.input';
import { QueryTypes } from 'sequelize';
import { User } from 'src/users/user.entity';
import { BETS_REPOSITORY, USERS_REPOSITORY } from 'src/constants';

@Injectable()
export class BetsService {
  constructor(
    @Inject(BETS_REPOSITORY)
    private betsRepository: typeof Bet,
    @Inject(USERS_REPOSITORY)
    private usersRepository: typeof User,
  ) {}
  async getBet(id: number): Promise<Bet> {
    return await this.betsRepository.findByPk(id, { raw: true });
  }
  async getBetList(): Promise<Bet[]> {
    return await this.betsRepository.findAll<Bet>({ raw: true });
  }
  async getBestBetPerUser(limit: number): Promise<Bet[]> {
    const query = `
    SELECT DISTINCT ON ("userId") *
    FROM "Bets"
    ORDER BY "userId", "betAmount" DESC
    LIMIT :limit;
  `;

    const replacements = { limit: limit };

    const results = await this.betsRepository.sequelize.query(query, {
      replacements,
      type: QueryTypes.SELECT,
    });

    return results as Bet[];
  }
  async createBet(createBetInput: CreateBetInput): Promise<Bet> {
    const { userId, betAmount, chance } = createBetInput;
    let transaction;
    try {
      transaction = await this.usersRepository.sequelize.transaction();

      const user = await this.usersRepository.findByPk(userId, {
        lock: transaction,
      });

      if (user.balance >= betAmount) {
        await user.decrement({ balance: betAmount }, { transaction });

        // Calculate the payout based on the chance (for simplicity, we'll assume it's a 50-50 chance)
        const win = Math.random() < chance;
        const payout = win ? betAmount * 2 : 0;

        const newBet = await this.betsRepository.create(
          {
            userId: userId,
            betAmount: betAmount,
            chance: chance,
            payout: payout,
            win: win,
          },
          { transaction },
        );

        await user.increment({ balance: payout }, { transaction });

        await transaction.commit();

        return newBet;
      } else {
        await transaction.rollback();
        throw new Error('Insufficient balance for the bet');
      }
    } catch (error) {
      if (transaction) await transaction.rollback();
      throw error;
    }
  }
}
