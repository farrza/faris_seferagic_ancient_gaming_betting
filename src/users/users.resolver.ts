import { NotFoundException } from '@nestjs/common';
import { Args, Query, Resolver } from '@nestjs/graphql';
import { User } from './models/user.model';
import { UsersService } from './users.service';

@Resolver()
export class UsersResolver {
  constructor(private readonly usersService: UsersService) {}

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  @Query((returns) => User)
  async getUser(@Args('id') id: number): Promise<User> {
    try {
      const user = await this.usersService.getUser(id);
      if (!user) {
        throw new NotFoundException(id);
      }
      return user;
    } catch (error) {
      console.error(`[getUser Resolver Error]: `, error);
      throw error;
    }
  }
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  @Query((returns) => [User])
  async getUserList(): Promise<User[]> {
    try {
      const users = await this.usersService.getUserList();
      if (!users) {
        throw new NotFoundException();
      }
      return users;
    } catch (error) {
      console.error(`[getUserList Resolver Error]: `, error);
      throw error;
    }
  }
}
