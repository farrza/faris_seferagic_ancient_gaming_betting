/* eslint-disable @typescript-eslint/no-unused-vars */
import { Field, Float, ID, ObjectType } from '@nestjs/graphql';

@ObjectType({ description: 'user' })
export class User {
  @Field((type) => ID)
  id: number;

  @Field()
  name: string;

  @Field((type) => Float)
  balance: number;
}
