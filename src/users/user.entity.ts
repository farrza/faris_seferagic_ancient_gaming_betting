import { Table, DataType, Column, Model } from 'sequelize-typescript';

@Table
export class User extends Model<User> {
  @Column({
    primaryKey: true,
    autoIncrement: true,
  })
  id: number;

  @Column
  name: string;

  @Column(DataType.DECIMAL(10, 2))
  balance: number;
}
