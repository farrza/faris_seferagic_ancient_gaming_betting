import { Inject, Injectable } from '@nestjs/common';
import { USERS_REPOSITORY } from 'src/constants';
import { User } from './user.entity';

@Injectable()
export class UsersService {
  constructor(
    @Inject(USERS_REPOSITORY)
    private usersRepository: typeof User,
  ) {}
  async getUser(id: number): Promise<User> {
    return await this.usersRepository.findByPk(id, { raw: true });
  }
  async getUserList(): Promise<User[]> {
    return await this.usersRepository.findAll<User>({ raw: true });
  }
}
